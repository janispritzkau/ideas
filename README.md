# Ideas

Repository for tracking my personal project ideas and goals.

- [ ] [DNS tunnel](https://gitlab.com/janispritzkau/dns-tunnel)
- [ ] [Spectral 2D raytracer](https://gitlab.com/janispritzkau/2d-raytracer)
    - [Snell's law of refraction](https://en.wikipedia.org/wiki/Snell%27s_law)
    - [Sellmeier equation](https://en.wikipedia.org/wiki/Sellmeier_equation)
- [ ] Animate different sorting algorithms
- [ ] Maze generator (recursive backtracking) / solver
    - https://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
- [ ] Ray-tracing a black hole
    - https://rantonels.github.io/starless/
- [ ] Modulation and demodulation of radio signals (FM, AM, QAM, PSK, ...)
- [ ] Path finding algorithms